var config = require('../config.json');
var spawn = require("child_process").spawn;

var PUMPING = 1;
var STOPPED = 0;

class RefillService {
    constructor(db) {
        this.db = db;
    }

    _formatDate(timestamp) {
        return new Date(timestamp).toISOString().slice(0, 19).replace('T', ' ');
    }

    /**
     * Fires off the relay and adds an entry into the database to say that we have started
     * if we have not already started.
     */
    start () {
        console.log("Starting Refill");
        return this._script('StartRelay.py').then((status) => {
            return this._record({
                state: PUMPING
            });
        });
    }

    stop () {
        console.log("Stopping Refill");
        return this._script('StopRelay.py').then((status) => {
            return this._record({
                state: STOPPED
            });
        });
    }

    _script (script) {
        return new Promise((resolve, reject) => {
            let proc = spawn('python', [__dirname + "/" + script]);
            // make sure proc starts properly by verifying its exit code
            proc.on('exit', function (code, signal) {
                if (code != 0)
                    throw {
                        message: "RefillService - Could not run " + script + ", must run as root",
                        success: false
                    };
                // it did, we can resolve
                console.log("Script Executed: " + code);
                resolve({
                    success: true
                })
            });
        });
    }

    /**
     * Returns the current action of the pump (on/off/when)
     */
    getCurrentPumpAction () {
        return new Promise((resolve, reject) => {
            console.log("Retrieving Pump Actions");
            this.db.query("SELECT * FROM PumpAction ORDER BY idPumpAction DESC LIMIT 1",
            function (error, results, fields) {
                if (error) {
                    reject(error);
                    return;
                };
                // if we have no results returned, return empty array
                if (results.length == 0) {
                    console.log("The pump has never been run");
                    resolve({
                        state: false,
                        timestamp: 0,
                        duration: null
                    });
                    return;
                }
                resolve(results[0]);
            });
        });
    }

    /**
     * Records the change in action in the database. 
     * State is 0 if off, 1 if pumping
     * @param {*} info 
     */
    _record (info) {
        return new Promise((resolve, reject) => {
            this.getCurrentPumpAction().then((currentAction) => {
                // if we already are doing our current action, do nothing.
                if (currentAction != null && currentAction.state == info.state) {
                    var event = {
                        state: info.state,
                        id: currentAction.id,
                        change: false
                    }
                    resolve(event);
                    return event;
                }
                // Gather general information about this pump action change
                var timestamp = Date.now();
                let timestampStr = this._formatDate(timestamp);
                var state = info.state;
                var duration = null;
                // calculate duration if the last one was stopped
                if (currentAction.state == STOPPED) {
                    duration = timestamp - new Date(currentAction.timestamp).getTime();
                }
                this.db.query("INSERT INTO PumpAction (timestamp, state, duration) VALUES (?, ?, ?)",
                    [timestampStr, state, duration], function (error, results, fields) {
                    if (error) {
                        reject(error);
                        return;
                    };
                    var event = {
                        state: info.state,
                        id: results.insertId
                    };
                    resolve(event);
                });
            });
        });
    }
};

module.exports = RefillService;