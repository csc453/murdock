var mysql = require('mysql');
var config = require('../config.json');
var RefillService = require('./RefillService');

class AnalyticsService {
    constructor() {
        this.level = 4;
        this.db = mysql.createConnection(config.database);
        this.db.connect(function(error) {
            if (error) {
                console.log(error);
            }
        });
        this.refillService = new RefillService(this.db);
    }

    _formatDate(timestamp) {
        return new Date(timestamp).toISOString().slice(0, 19).replace('T', ' ');
    }
    
    insertWaterLevel(waterLevel) {
        var timestamp = this._formatDate(Date.now());
    
        return this.refillService.getCurrentPumpAction().then((pump) => {
            return new Promise((resolve, reject) => {
                console.log("Inserting Water Level");
                this.db.query("INSERT INTO WaterLevel (timestamp, state, level) VALUES (?, ?, ?)",
                    [timestamp, pump.state, waterLevel], function (error, results, fields) {
                    if (error) {
                        console.log("Could not insert water level...");
                        reject(error);
                    }
                    console.log("Inserted Water Level: ", results);
                    resolve({
                        pump: pump,
                        waterLevel: waterLevel,
                        results: results
                    });
                });
            });
        }).then((info) => {
            let pump = info.pump;
            let waterLevel = info.waterLevel;
            let results = info.results;
            console.log("Checking Pump Analytics: ", pump, waterLevel, results);
            if (!pump.state && waterLevel > this.level) { //TODO Figure out how much is 1"
                console.log("Pump needs to be started.");
                pump.state = true;
                this.refillService.start();
                //TODO predict how long to keep thing on
            } else if (pump.state && waterLevel <= this.level) { //TODO put in 1"
                console.log("Pump needs to be stopped.");
                pump.state = false;
                this.refillService.stop();
            }
            return results;
        });
    };
    
    //Gets all the water level readings for the given date
    getDayWaterLevels(date) {
        // The start date is when the user specified at midnight
        let startDate = new Date(date);
        startDate = this._formatDate(startDate);
        // The end date is 24 hours later
        let endDate = new Date(date);
        endDate.setDate(endDate.getDate() + 1);
        endDate = this._formatDate(endDate);
        console.log("Fetching water levels for day: ", date);
        return new Promise((resolve, reject) => {
            this.db.query("SELECT * FROM WaterLevel WHERE timestamp >= ? AND timestamp <= ?",
                [startDate, endDate], function (error, results, fields) {
                    if (error) {
                        reject(error);
                    }
                    console.log('res', results);
                    resolve(results);
                });
        });
    };
    
    //Gets the current water level reading
    getCurrentWaterLevel() {
        return new Promise((resolve, reject) => {
            this.db.query("SELECT * FROM WaterLevel ORDER BY idWaterLevel DESC LIMIT 1", function (error, results, fields) {
                    if (error) {
                        reject(error);
                    }
                    resolve(results);
                });
        });
    };
    
    //Gets predicted pump time by averaging times for the last week
    getPredPumpTime() {
        return new Promise(function(resolve, reject) {
            this.db.query("SELECT duration FROM PumpAction WHERE (state = '0') AND (timestamp BETWEEN timestamp(DATE_SUB(NOW(), INTERVAL 1 WEEK)) AND timestamp(NOW()))",
                function (error, results, fields) {
                    if (error) {
                        reject(error);
                    };
                    var sum = 0;
                    for (let i = 0; i < results.length; i++) {
                        sum += results[i].duration;
                    }
                    resolve(sum / results.length);
                });
        });
    };
}
module.exports = AnalyticsService;

//////////////////////////////The following are not in use

// updateDailyWaterLevels = function () {
//     return new Promise(function(resolve, reject) {
//         this.db.query("INSERT INTO DailyWaterLevels (level, timestamp) VALUES (?, ?)",
//             [pumpkey, waterLevel, timestamp], function (error, results, fields) {
//                 if (error) {
//                     reject(error);
//                 };
//                 resolve(results);
//             });
//         // this.insertWaterLevel(waterLevel); //Do this???
//     });
// };
//
// //Gets the current day's water level average
// getWaterLevelAverage = function () {
//     return new Promise(function(resolve, reject) {
//         this.db.query("SELECT level FROM WaterLevel WHERE timestamp >= CURDATE()",
//             function (error, results, fields) {
//                 if (error) {
//                     reject(error);
//                 };
//                 var sum = 0;
//                 for (let i = 0; i < results.length; i++) {
//                     sum += results[i].level;
//                 }
//                 resolve(sum / results.length);
//             });
//     });
// };
//
// //Gets rate of change in the last minute
// getRatePerMinute = function () {
//     return new Promise(function(resolve, reject) {
//         this.db.query("SELECT diff FROM WaterLevelDiffs WHERE timestamp BETWEEN timestamp(DATE_SUB(NOW(), INTERVAL 1 MINUTE)) AND timestamp(NOW())",
//             function (error, results, fields) {
//                 if (error) {
//                     reject(error);
//                 };
//                 var sum = 0;
//                 for (let i = 0; i < results.length; i++) {
//                     sum += results[i].diff;
//                 }
//                 resolve(sum);
//             });
//     });
// };