import Adafruit_BBIO.GPIO as GPIO
import os, sys

# make sure we are root
if os.geteuid() != 0:
    sys.exit(1)

relayPin = "P9_12"
GPIO.setup(relayPin, GPIO.OUT)
GPIO.output(relayPin, GPIO.HIGH)