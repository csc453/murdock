import Adafruit_BBIO.GPIO as GPIO
import time

# from Sensor import *
trigPin = "P9_23"
echoPin = "P9_15"

def setupPins(debug=False):
    if debug:
        print "Setup Pins"
    GPIO.setup(trigPin, GPIO.OUT)
    GPIO.output(trigPin, GPIO.LOW)
    if debug:
        print "Waiting for trig pin to reset"
    time.sleep(2)
    GPIO.setup(echoPin, GPIO.IN)
    
def cleanUp(debug=False):
    GPIO.cleanup()
    
def trigLow(debug=False):
    if debug:
        print "Trigger Low"
    GPIO.output(trigPin, GPIO.LOW)
    
def trigHigh(debug=False):
    if debug:
        print "Trigger high"
    GPIO.output(trigPin, GPIO.HIGH)
    
def trigPulse(pulse=0.00001, debug=False):
    trigLow()
    if debug:
        print "Pulse (high)"
    trigHigh()
    time.sleep(pulse)
    trigLow()
    if debug:
        print "Pulse finished"
    
def waitForPulse(debug=False):
    if debug:
        print "Waiting for pulse"
    GPIO.wait_for_edge(echoPin, GPIO.RISING)
    
def pollForPulse(wait, debug=False):
    # convert from ms to seconds
    wait = wait / 1000
    start = time.time()
    cur = time.time()
    if debug:
        print "Polling for pulse"
    while GPIO.input(echoPin) == 0:
        cur = time.time()
        if cur - start > wait:
            print "No    pulse: " + str(wait) + "s"
            return False
    start = time.time()
    while GPIO.input(echoPin) == 1:
        cur = time.time()
    diff = cur - start
    print "Found pulse: " + str(diff)
    return True
    
def runTest(loops, sleep=0, pulse=0.00001, debug=False):
    setupPins()
    countSuccess = 0
    countFail = 0
    for t in range(0, loops):
        if debug:
            print "\nTest " + str(t) + " \n------"
        trigPulse(pulse)
        if pollForPulse(150):
            countSuccess += 1
        else:
            countFail += 1
        if debug:
            print "--- Test finished ---"
        if sleep != 0:
            time.sleep(sleep)
    print "Successes: {} Fail: {}".format(countSuccess, countFail)
    cleanUp()

    
