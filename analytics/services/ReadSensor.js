var b = require('bonescript');

function readSensor() {
    b.analogRead('P9_16', printValue);
}

function printValue(x) {
    console.log(x);
}

// read from sensor every 1 second
setInterval(readSensor, 1000);
