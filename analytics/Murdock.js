var express = require('express');
//var bonjour = require('bonjour')();
var app = express();
var config = require('./config.json');
var AnalyticsService = require('./services/AnalyticsService');

var analyticsService = new AnalyticsService();

// Tell nodeJS to read in POST statements as JSON or form-encoded data (for old style posts)
app.use([express.json(), express.urlencoded({ extended: true })]);
app.use('/ui', express.static('frontend/ui/'));
app.use('/gstatic', express.static('frontend/gstatic/'));
app.get('/', function (req, res) {
    res.sendFile(__dirname + '/frontend/app.html');
});

// Show our API 'status'
app.get(config.server.api + "status", function(req, res) {
    res.send({
        alive: true,
        status: "ok"
    });
});

app.put(config.server.api + "ideal/waterlevel/:level", function(req, res) {
    try {
        let ideal = parseFloat(req.params.level);
        console.log("Changing ideal water level: " + ideal);
        analyticsService.level = ideal;
        res.send({
            message: "Ideal water level set to " + ideal,
            success: true
        });
    } catch (err) {
        res.status(400).send({
            message: "Cannot set water level to " + req.params.level,
            success: false
        })
    }
});

//Stores the waterlevels in the database
app.post(config.server.api + "waterlevel", function(req, res) {
    console.log("whoa body", req.body);
    if (!req.body.level) {
        res.status(400).send({
            message: "A valid water level must be specified in the body.",
            success: false
        });
        return;
    };
    analyticsService.insertWaterLevel(req.body.level).then(function(results) {
        res.send({
            message: "The water level has been stored.",
            success: true,
            results: results
        })
    }).catch(function(error) {
        res.status(500).send({
            message: error,
            success: false
        });
    });
});

//Get the current water level
app.get(config.server.api + "waterlevel/current", function (req, res) {
    analyticsService.getCurrentWaterLevel().then(function(results) {
        res.send({
            message: "The current water level has been retrieved.",
            success: true,
            results: results
        });
    }).catch(function(error) {
        res.status(500).send({
            message: error,
            success: false
        });
    });
});

//Get all the Water levels for a given date
app.get(config.server.api + "waterlevel/:date", function (req, res) {
    if (!req.params.date) {
        res.status(400).send({
            message: "A valid date must be specified in the body.",
            success: false
        });
        return;
    };
    analyticsService.getDayWaterLevels(req.params.date).then(function(results) {
        res.send({
            message: "The water level data for the date has been retrieved.",
            success: true,
            date: req.params.date,
            results: results
        });
    }).catch(function(error) {
        res.status(500).send({
            message: error,
            success: false
        });
    });
});

//Get the predicted pump time
app.get(config.server.api + "predict/pumptime", function (req, res) {
    analyticsService.getPredPumpTime().then(function(results) {
        res.send({
            message: "The predicted pump time has been retrieved.",
            success: true,
            results: results
        });
    }).catch(function(error) {
        res.status(500).send({
            message: error,
            success: false
        });
    });
});

// //Get the current day's water level average
// app.get(config.server.api + "waterlevel/average/:date", function (req, res) {
//     analyticsService.getWaterLevelAverage().then(function(results) {
//         res.send({
//             message: "The water level average has been retrieved.",
//             success: true,
//             results: results
//         });
//     }).catch(function(error) {
//         res.ststus(500).send({
//             message: error,
//             success: false
//         });
//     });
// });

// //Get the rate of change in the last minute
// app.get(config.server.api + "rate/minute", function (req, res) {
//     analyticsService.getRatePerMinute().then(function(results) {
//         res.send({
//             message: "The rate per minute has been retrieved.",
//             success: true,
//             results: results
//         });
//     }).catch(function(error) {
//         res.ststus(500).send({
//             message: error,
//             success: false
//         });
//     });
// });

app.listen(config.server.port, function() {
    console.log("API listening on pert " + config.server.port);
});
/*
// Configure mDNS to create a local hostname for our server
let test = bonjour.publish({ name: 'murdock', type: 'http', port: config.server.port });
setTimeout(()=> {console.log(test)}, 2000);
bonjour.find({ type: 'http' }, function (service) {
    console.log('Found an HTTP server:', service)
});
*/