var express = require('express');
var app = express();
var config = require('./config.json');

// Show the Hugo static site
app.use(config.server.site, express.static('docs/public'));
app.use([express.json(), express.urlencoded({ extended: true })]);

// Stores the IP of the beaglebone that the other beaglebone can use to connect
var beagleboneIp = "";
app.get("/beaglebone", function(req, res) {
    res.send({
        ip: beagleboneIp,
        success: true
    });
});
app.put("/beaglebone", function(req, res) {
    console.log(req.body);
    if (!('ip' in req.body)) {
        res.send({
            success: false,
            message: "no ip provided"
        });
    }
    beagleboneIp = req.body.ip;
    res.send({
        success: true,
        message: "New IP: " + beagleboneIp
    });
});

app.listen(config.server.port, function() {
    console.log("Murdock listening on pert " + config.server.port);
    console.log("Serving site on " + config.server.site);
});