---
title: "Status"
date: 2018-01-27T15:42:17+01:00
anchor: "status"
weight: 60
---

### Hardware
The circuitry for the entire system has been finished. Below is the relay and sensor circuit.

<img src = "circuit.jpg" alt = "Hardware work" width = "100%">

### Software
There are four major software systems:

1. Measurement Service
2. Analytics Service
3. Refill Service
4. Web Application

All of the systems are completed and implemented.