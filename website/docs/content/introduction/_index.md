---
title: "Murdock"
date: 2018-01-27T15:42:17+01:00
anchor: "introduction"
weight: 10
---

## An Aquarium Water Level Internet of Things Device

Setting up and maintaining an aquarium can be a difficult task for any fish owner. Mistakes can cause the death of many fish and aquarium dwellers. The ability to easily and accurately monitor and maintain water level is very important to the wellbeing of the tank.

{{% block note %}}
The project is completed.
{{% /block %}}
