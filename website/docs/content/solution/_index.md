---
title: "Envisioned Solution"
date: 2018-01-27T15:42:17+01:00
anchor: "solution"
weight: 40
---

The Aquarium Water Level IoT Device, <b>Murdock</b>, is composed of four functional components: <i>measure, monitor, analyze,</i> and <i>refill</i>. These four functions will be implemented through three software services: The <i>measurement service</i>, the <i>analytics service</i>, and the <i>refilling service</i>. The measurement service is responsible for retrieving information provided through analog signals from the water level sensor and mapping them to consumable data for the analytics service. The refilling service is responsible for providing an interface to modify the state of the valve in a semantically correct context. Lastly, the analytics service performs intelligent governance between the measurements received and when the refilling service is activated. Monitoring is simply implemented by retrieving persisted data via the measurement service exposed through a <i>web server</i>.
