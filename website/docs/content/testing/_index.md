---
title: "Test Plan"
date: 2018-01-27T15:42:17+01:00
anchor: "testing"
weight: 50
---

The comprehensive test plan that will ensure Murdock is working appropriately
can be viewed attached to the final report [here](https://drive.google.com/open?id=1166DJtynTh_ncwOSROK_q59uSeq9W34e). 