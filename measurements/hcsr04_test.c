#include <stdio.h>
#include <unistd.h>
#include <time.h>

void main() {
   FILE *fptr;

   fptr = fopen("sensor_data.txt","w");

   int i = 0;

   while (i++ < 20) {
      if (i == 20) {
         fprintf(fptr, "123 inches");
      } else {
         fprintf(fptr, "123 inches,");
      }
   }

   fclose(fptr);
}
