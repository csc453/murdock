from subprocess import call
from datetime import datetime
from time import sleep
import json
import requests

print("[MEASUREMENT SERVICE STARTED]")

url = "http://beaglebone.local:2018/api/waterlevel" # API endpoint to post water level data to

def mean(numbers):
    return float(sum(numbers)) / max(len(numbers), 1)

try:
   while True:
      print ("[{}] Reading HC-SR04 sensor measurements and writing to sensor_data.txt...".format(str(datetime.now())))
      call(["./hcsr04"]) # Call C program that starts the HC-SR04 sensor measurement process
                         # call() function waits for C program execution to finish before resuming this script

      file_object  = open("sensor_data.txt", "r") # C program writes measurements to this file
      file_content = file_object.read() # Read measurements from sensor_data.txt

      post_data = {}
      post_data["time"] = str(datetime.now()) # Store current time in JSON object
      # Average the data retrieved from the script
      data = [ float(x) for x in file_content.split(',') ]
      level = mean(data)
      print("{} water level found".format(str(level)))
      post_data["level"] = level # Store measurements in JSON object

      #post_data_json = json.dumps(post_data)

      r = requests.post(url, json=post_data) # Make POST request to API endpoint

      print("[{}] POST sensor data to {}: {}".format(str(datetime.now()), url, r.status_code))
      print("Waiting...")
      sleep(1);

except KeyboardInterrupt:

   print("[MEASUREMENT SERVICE STOPPED]")
