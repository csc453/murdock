# Team 1

**Anne Church** (lead)

Kenny Jones

Wesley Coats

Brandon Nguyen

# Development Environment

## Murdock Analytics Service API & RefillService

The murdock API is written in javascript for [NodeJS](https://nodejs.org/en/). 

### Setup

1. Install NodeJS locally [Download here](https://nodejs.org/en/)

2. Clone the repository (https or ssh depending on your personal auth)

3. Use the node package manager *npm* to install the dependencies:

```bash
cd {analytics}
npm install
```

4. Install Python 2.7

5. Use PIP to install Adafruit's GPIO library

6. Run the server

```bash
sudo node Murdock.js
```


## Measurement Service

The measurement service can be run directly with Python 2.7

1. Install Python 2.7

2. Use PIP to install Requests library

3. Run the service

```bash
cd {measurements}
sudo python measurement_service.py
```

## Murdock Website

The website uses a static-site framework called [Hugo](https://gohugo.io/). 
The hugo config files are inside of docs/, and the built static site is inside docs/public/.

### Setup

1. Install Hugo locally [Download here](https://gohugo.io/getting-started/installing/)

2. Clone this repository

3. Run hugo to build public/ if it isn't already built

```bash
cd {{top}/website/docs/}
hugo
```

Note:
Hugo can also serve static content locally and constantly rebuild --

```bash
cd {{top}/website/docs/}
hugo serve
```

# Deployment Server

The Murdock API is deployed onto the private server owned by the team.

It is surfaced here: [murdock.ottermatic.io](http://murdock.ottermatic.io/)

## Managing & Updating

The code is stored on /srv/www/murdock

[pm2](http://pm2.keymetrics.io/) is used to manage NodeJS applications on this server.

Install pm2 globally

```bash
npm install pm2 -g
```

Run the application via pm2

```bash
pm2 start Murdock.js
```

Likewise, it can be restarted as such:

```bash
pm2 restart Murdock.js
```